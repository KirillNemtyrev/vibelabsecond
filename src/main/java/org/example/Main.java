package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Укажите температуру окружающей среды:");
        int temperature = scanner.nextInt();
        System.out.println("Укажите скорость ветра:");
        int wind = scanner.nextInt();
        System.out.println("Укажите идёт ли щас дождь(0-нет дождя, 1-есть дождь): ");
        int rain = scanner.nextInt();

        if(rain == 1){
            System.out.println("Сейчас идёт дождь, лучше посидите дома.");
        } else {

            if (temperature >= 10 && temperature <= 15) {

                if (wind <= 5){
                    System.out.println("Погодка шикарная, можете спокойно одевать шорты и футболку.");
                } else {
                    System.out.println("Скорость ветра больше 5 м/с, можете идти гулять, только оденьтесь потеплее.");
                }

            } else if (temperature > 0 && temperature <= 10) {

                if (wind <= 5){
                    System.out.println("Надевайтесь потеплее, кофту и штаны, и можете идти гулять");
                } else {
                    System.out.println("Будет небольшой ветерок, вам лучше чуть-чуть теплее одеться.");
                }

            } else {

                if (wind <= 5){
                    System.out.println("На улице очень жарко, можете смело идти даже купаться");
                } else {
                    System.out.println("Купаться бы не советовал, но прогуляться стоит.");
                }

            }

        }


    }
}